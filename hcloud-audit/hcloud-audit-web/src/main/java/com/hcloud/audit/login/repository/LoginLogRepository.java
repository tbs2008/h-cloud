package com.hcloud.audit.login.repository;

import com.hcloud.audit.login.entity.LoginLogEntity;
import com.hcloud.common.crud.repository.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Repository
public interface LoginLogRepository extends BaseRepository<LoginLogEntity> {
}
