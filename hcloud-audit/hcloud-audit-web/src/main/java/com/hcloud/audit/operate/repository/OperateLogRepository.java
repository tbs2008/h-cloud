package com.hcloud.audit.operate.repository;

import com.hcloud.audit.operate.entity.OperateLogEntity;
import com.hcloud.common.crud.repository.BaseRepository;
import org.springframework.stereotype.Repository;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Repository
public interface OperateLogRepository extends BaseRepository<OperateLogEntity> {
}
