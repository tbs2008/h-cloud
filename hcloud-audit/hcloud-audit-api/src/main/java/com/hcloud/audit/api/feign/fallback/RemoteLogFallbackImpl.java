package com.hcloud.audit.api.feign.fallback;

import com.hcloud.audit.api.bean.LoginLog;
import com.hcloud.audit.api.bean.OperateLog;
import com.hcloud.audit.api.feign.RemoteLogService;
import com.hcloud.common.core.base.HCloudResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Auther hepangui
 * @Date 2018/11/8
 */
@Slf4j
@AllArgsConstructor
public class RemoteLogFallbackImpl implements RemoteLogService {
    private final Throwable cause;


    @Override
    public HCloudResult saveLog(OperateLog operateLog) {
        log.error("feign 保存日志失败:{}", operateLog, cause);
        return null;
    }

    @Override
    public HCloudResult saveLoginLog(LoginLog loginLog) {
        log.error("feign 保存日志失败:{}", loginLog, cause);
        return null;
    }
}
