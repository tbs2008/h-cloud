package com.hcloud.auth.service.impl;

import com.hcloud.auth.api.user.HcloudUserDetails;
import com.hcloud.auth.api.config.LoginType;
import com.hcloud.auth.service.ThirdUserService;
import com.hcloud.common.core.base.HCloudResult;
import com.hcloud.common.social.exception.QqNotBindException;
import com.hcloud.common.social.exception.WeixinNotBindException;
import com.hcloud.common.social.service.GiteeInfoService;
import com.hcloud.common.social.service.QQInfoService;
import com.hcloud.common.social.service.WeixinInfoService;
import com.hcloud.system.api.feign.authority.RemoteAuthorityService;
import com.hcloud.system.api.feign.user.RemoteUserService;
import com.hcloud.common.core.base.User;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * 查询用户的方法
 */
@Primary
@Service
@Slf4j
@AllArgsConstructor
public class HcloudUserDetailsService implements UserDetailsService, ThirdUserService {

    private final RemoteUserService remoteUserService;

    private final RemoteAuthorityService remoteAuthorityService;

    private final WeixinInfoService weixinInfoService;

    private final QQInfoService qqInfoService;

    private final GiteeInfoService giteeInfoService;

    @Override
    public UserDetails loadUserByUsername(String account) throws UsernameNotFoundException {
        return this.loadUser(account, LoginType.pass);
    }

    @Override
    public HcloudUserDetails loadUser(String para, LoginType type) {
        HCloudResult<User> userHCloudResult;
        switch (type) {
            case mobile:
                log.info("开始根据手机号查询用户：{}", para);
                userHCloudResult = remoteUserService.getUserByMobile(para);
                break;
            case qq:
                log.info("开始根据qq查询用户：{}", para);
                String openId = qqInfoService.getOpenId(para);
                userHCloudResult = remoteUserService.getUserByQq(openId);
                if(userHCloudResult == null || userHCloudResult.getData() == null){
                    throw new QqNotBindException();
                }
                break;
            case weixin:
                log.info("开始根据code查询openId：{}", para);
                String openId1 = weixinInfoService.getOpenId(para);
                log.info("开始根据微信OpenId查询用户：{}", openId1);
                userHCloudResult = remoteUserService.getUserByWeixin(openId1);
                if(userHCloudResult == null || userHCloudResult.getData() == null){
                    throw new WeixinNotBindException();
                }
                break;
            case gitee:
                log.info("gitee账户登录");
                String id = giteeInfoService.getOpenId(para);
                userHCloudResult = remoteUserService.getUserByAccount("test");
                break;
            default:
                log.info("开始根据账号查询用户：{}", para);
                userHCloudResult = remoteUserService.getUserByAccount(para);
                break;
        }
        log.info("用户：{}", userHCloudResult);
        if (userHCloudResult != null && userHCloudResult.getData() != null) {
            HcloudUserDetails myUserDetail = new HcloudUserDetails(userHCloudResult.getData());
            String roles = userHCloudResult.getData().getRole();
            HCloudResult<List<String>> byRoleIds = remoteAuthorityService.findByRoleIds(roles);
            if (byRoleIds != null && byRoleIds.getData() != null) {
                Set<SimpleGrantedAuthority> set = new HashSet<>();
                for (String datum : byRoleIds.getData()) {
                    if (datum != null && !"".equals(datum)) {
                        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(datum);
                        set.add(simpleGrantedAuthority);
                    }
                }
                myUserDetail.setAuthorities(set);
            }
            return myUserDetail;
        } else {
            log.error("找不到用户{}", para);
            return null;
        }
    }
}
