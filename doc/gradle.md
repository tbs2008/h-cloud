##### 导入已经下载或检出的项目，选择gradle模式
    务必选择5.0版本，以及JDK11进行构建
    项目导入之后就会自动构建了
![图片](img/gradle/image1.png)
![图片](img/gradle/image2.png)
![图片](img/gradle/image3.png)
![图片](img/gradle/image4.png)
![图片](img/gradle/image5.png)
![图片](img/gradle/image6.png)
##### 刷新按钮可以重新构建
    在更新了依赖之后请刷新
![图片](img/gradle/image7.png)
##### buildJar用来生成运行的jar包
    生成后的路径在对应项目的build/libs/文件夹下
![图片](img/gradle/image8.png)
##### 对了，开启这个，lombok的注解才能生效
![图片](img/gradle/img8.png)
