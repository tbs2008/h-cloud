package com.hcloud.common.crud.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Data
@MappedSuperclass//该类是一个基础类
public class BaseEntity {
    @Id
//    @Column(name="id" , nullable = false)
    @GeneratedValue(generator = "idGenerator")
    @GenericGenerator(name = "idGenerator", strategy = "uuid")
    protected String id;

    protected Date createTime;

    protected Date updateTime;

}
